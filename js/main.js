// console cross-browser compat
var console = console || {};
console.log = console.log || function () {};

function loadScript(url, callback) {
    // Adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    console.log("Loading: " + url);
    script.src = url;

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    script.onreadystatechange = callback;
    script.onload = callback;

    // Fire the loading
    head.appendChild(script);
}

function loadScripts(scripts, callback) {
	console.log("[loadScripts] scripts=", scripts);
	if (scripts.length > 1) {
		loadScripts(scripts.slice(0, scripts.length - 1), function () {
			loadScripts(scripts.slice(scripts.length - 1), callback);
		});
	} else {
		loadScript(scripts[0], callback)
	}
}

function loadDependancies(callback) {
	loadScripts(['http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
			'http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js',
			'/js/jquery.linkify-1.0-min.js'],
			function() {
				callback();
			});
};

function init(callback) {
	var div = document.createElement("div");
	div.innerHTML = "<!--[if lt IE 9]><i></i><![endif]-->";
	var isIeLessThan9 = (div.getElementsByTagName("i").length == 1);
	if (isIeLessThan9) {
		loadScripts('http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js',
				 'http://oss.maxcdn.com/respond/1.4.2/respond.min.js',
				 function() {
					loadDependancies(callback);
				});
	} else {
		loadDependancies(callback);
	}
}